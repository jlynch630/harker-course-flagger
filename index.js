var firebase = require("firebase");
var https = require("https");

// Initialize Firebase
var config = {
	apiKey: "AIzaSyDGzHdJ-4B35kuShuJCgmHhkbBy_nMCvy4",
	authDomain: "harker-courses.firebaseapp.com",
	databaseURL: "https://harker-courses.firebaseio.com",
	projectId: "harker-courses",
	storageBucket: "harker-courses.appspot.com",
	messagingSenderId: "125151666633"
};
firebase.initializeApp(config);
var ref = firebase.database().ref("flagged_reviews");
ref.on("child_added", (snapshot) => {
	var newVal = snapshot.val();
	if (newVal) {
		var object = {
			content: `A review has been flagged for potentially inappropriate content.\nTitle:${newVal.title || "(None)"}\nText:${newVal.review || "(None)"}\nAuthor:${newVal.author || "Anonymous"}\nCourse ID:${newVal.courseId}\nFlagged By:${newVal.flaggedBy}\n\nView at https://harker-courses.firebaseio.com/reviews/${newVal.courseId}`,
		};
		var data = JSON.stringify(object);

		// Set up the request
		var request = https.request({
			host: 'discordapp.com',
			port: '443',
			path: '/api/webhooks/452339656374091776/497DENCetIfI8-rK6LRqeMlZVQEdndfWDCglbebOtadakaE2I_2QkSk3O8rjh4Dtft3a',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Content-Length': Buffer.byteLength(data)
			}
		});

		// post the data
		request.write(data);
		request.end();
	}
});